from math import radians, sin, cos, asin, sqrt, degrees
import json
import requests


class PlaneList:
    """
        sher_latitude: the Sheremetyevo airport's latitude
        sher_longitude: the Sheremetyevo airport's longitude
        km_in_degree_lat: Number of kilometers in 1 degree
        radius: Search distance
    """

    def __init__(self):
        self.sher_latitude = 55.966786
        self.sher_longitude = 37.415685
        self.km_in_degree_lat = 111
        self.radius = 100

    def distance(self, lon1, lat1, lon2, lat2):
        """
        Calculate the  distance between two points
        on the earth (specified in decimal degrees)
        :param lon1: first point longitude
        :param lat1: first point latitude
        :param lon2: second point longitude
        :param lat2: second point latitude
        :return: distance between two points in meters
        """
        earth_radius = 6371
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a))
        dec_m = earth_radius * c
        return dec_m

    def getlist(self):
        """

        :return: list with information about planes in radius 100km near the Sheremetyevo International Airport
        """
        lat_min = self.sher_latitude - (self.radius / self.km_in_degree_lat)
        lat_max = self.sher_latitude + (self.radius / self.km_in_degree_lat)
        long_max = self.sher_longitude + self.radius / abs(cos(radians(self.sher_latitude)) * self.km_in_degree_lat)
        long_min = self.sher_longitude - self.radius / abs(cos(radians(self.sher_latitude)) * self.km_in_degree_lat)
        params = {'lamin': lat_min, 'lomin': long_min, 'lamax': lat_max, 'lomax': long_max}
        try:
            resp = requests.get('https://opensky-network.org/api/states/all', params=params)
            resp = resp.json()['states']
            print(len(resp))
            if resp:
                return [i for i in resp if self.distance(i[5], i[6], self.sher_longitude, self.sher_latitude) <= 100]
            else:
                return 'There is no planes in radius 100 km near the Sheremetyevo airport'
        except Exception:
            return ['Error']

print(PlaneList().getlist())